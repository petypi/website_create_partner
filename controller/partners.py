# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.http import Controller, route, request
from wtforms import Form, StringField, SelectField, BooleanField
from wtforms.validators import Required, Email
from openerp.tools.translate import _ as _t
from speaklater import make_lazy_gettext


_ = make_lazy_gettext(lambda: _t)


class Partners(Controller):

    @route('/partners/create', website=True, csrf=False)
    def create(self, **post_data):
        form = PartnerForm(request.httprequest.form)
        form.country_id.choices = [
            (country.id, country.name)
            for country in request.env['res.country'].search([])
        ]
        if request.httprequest.method == 'POST' and form.validate():
            request.env['res.partner'].create(dict([
                (field_name, field.data)
                for field_name, field in form._fields.iteritems()
            ]))
            return request.render(
                'website_create_partner.success',
                {'name': form.name.data})
        return request.render(
            'website_create_partner.partner_new',
            {'form': form})


class PartnerForm(Form):
    name = StringField(_('Name'),
        [Required(_('Please enter a name for the Partner'))])
    email = StringField(_('Email'), [
        Email(_('This is not a valid email')),
        Required(_('Please enter an email for the Partner'))])
    is_company = BooleanField(_('Is the partner a company?'))
    street = StringField(_('Address'))
    city = StringField(_('City'))
    country_id = SelectField(_('Country'), coerce=int)
