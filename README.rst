=================
Create Partner
=================

Simple module for creating partners through the website. This module demonstrates the use of WTForms in Odoo. 
For more details on how to use WTForms in Odoo please check our blog post (https://www.hbee.eu/en-us/blog/archive/2015/6/12/odoo-forms/).